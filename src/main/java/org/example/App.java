package org.example;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.time.Duration;
import java.time.Instant;

class App extends JFrame {


    private static  int max = 100000;
    private static int stepsize = 10000;







    public App(String title, XYDataset dataset) {
        super(title);



        // Create chart
        JFreeChart chart = ChartFactory.createXYLineChart(
                "alo primzahlenfinder zeit relator",
                "grösse des zahlenraums",
                "millisekunden",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        // Create Panel
        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);
    }






    public static void main(String[] args) {



        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series = new XYSeries("primzahelnfinder");
        System.out.println("Suche Primzahlen");


        for (int l = 0; l<=max;l=l+stepsize) {
            Instant start = Instant.now();


// l definiert den maximalen zahlenraum 0 bis l
            for (int i = 1; i < l; i++) {
                boolean prime = true;
                for (int y = 1; y < l; y++) {
                    if (i % y == 0 && i != y && y != 1)  // wenn die divsion einen rest hat und nicht durch sich selbst oder durch 1 geteilt wurde, dann ist es keine primzahl
                    {
                        prime = false;
                        break;
                    }
                }
                if (prime) {
                   // System.out.println("Primzahl gefunden: " + i);
                }
            }

            Instant finish = Instant.now();
            long timeElapsed = Duration.between(start, finish).toMillis();
            series.add(l, timeElapsed);
            System.out.println(l+" fertig in " + timeElapsed + " Millisekunden :)");
        }

        dataset.addSeries(series);

        SwingUtilities.invokeLater(() -> {
            App example = new App("primefinder",dataset);
            example.setSize(800, 400);
            example.setLocationRelativeTo(null);
            example.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            example.setVisible(true);
        });

    }
}
